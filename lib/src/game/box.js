'use strict';
import { app, Graphics } from './../pixi_application';

export default class Box {

    constructor(x, y, width, height, color) {
        this.rectangle = new Graphics();
        this.rectangle.lineStyle(2, color, 1);
        this.rectangle.drawRect(0, 0, width, height);
        this.rectangle.endFill();
        this.rectangle.x = x;
        this.rectangle.y = y;

        app.stage.addChild(this.rectangle);
    }
}
