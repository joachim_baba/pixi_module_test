
export default class GameState {
    constructor () {
        this.lifeMax = 3;
        this.life = 3;
    }

    registerLifeBarCallBack(callback) {
        this.lifeBarCallBack = callback;
    }

    decrementLife() {
        if(this.life > 0) {
            this.life--;
            this.lifeBarCallBack(this.life / this.lifeMax);
        } else {
            this.gameOver();
        }
    }

    gameOver() {
        console.log('game over');
    }
}
