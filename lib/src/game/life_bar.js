import { app, Graphics } from './../pixi_application';

export default class LifeBar {
    constructor() {
        this.width = 200;
        this.height = 10;
        this.bar = new Graphics();
        this.fill(1);
        this.bar.x = 5;
        this.bar.y = 5;

        app.stage.addChild(this.bar);
    }

    fill(portion) {
        this.bar.clear();
        this.bar.beginFill(0x3dbc12);
        this.bar.drawRect(0, 0, this.width * portion, this.height);
        this.bar.endFill();
    }
}
