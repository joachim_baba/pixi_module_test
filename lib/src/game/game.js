'use strict';
import Player from './player/player';
import Ground from './ground';
import {hitTestRectangle} from '../utils/hit_test';
import GameState from './game_state';
import LifeBar from './life_bar';
import ObstacleManager from './obstacle_manager';
import Background from './background';
import DifficultyManager from './difficulty_manager';

export default class Game {
    constructor() {
        this.background = new Background();
        this.player = new Player();
        this.ground = new Ground();
        this.gameState = new GameState();
        this.lifeBar = new LifeBar();
        this.difficultyManager = new DifficultyManager();

        this.gameState.registerLifeBarCallBack((life) => this.lifeBar.fill(life));
        this.obstacleManager = new ObstacleManager(() => this.obstacleCallback());
    }

    update(deltaTime) {
        try {
            this.player.update(deltaTime);
            this.obstacleManager.updateObstacles(deltaTime);
            this.background.update(deltaTime);
            this.difficultyManager.update(deltaTime);
        } catch (error) {
            console.log(error);
        }
    }

    collisionUpdate() {
        try {
            let hitInfos = hitTestRectangle(this.player.collider, this.ground.rectangle);

            if(hitInfos.collides) {
                this.player.sprite.y -= hitInfos.distY;
                this.player.onGroundHit();
            }

            this.obstacleManager.collisionUpdate(this.player);
        } catch (error) {
            console.log(error);
        }
    }

    obstacleCallback () {
        if(!this.player.isHurtActive) {
            this.gameState.decrementLife();
            this.player.onObstacleHit();
        }
    }
}

