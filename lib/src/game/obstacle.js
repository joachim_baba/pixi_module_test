import { globalSpeedMultiplier } from '../utils/game_infos';
import Mob from './mob';

export default class Obstacle extends Mob {
    constructor (x, y) {
        super(x, y);
    }

    move(deltaTime) {
        this.sprite.x -= deltaTime * (Obstacle.speed * globalSpeedMultiplier);
    }
}

Obstacle.speed = 3;
