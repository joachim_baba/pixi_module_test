'use strict';
import { app, Sprite, resources } from '../pixi_application';
import { globalSpriteScale } from '../utils/game_infos';

export default class Mob {

    constructor(x, y) {
        this.texture = resources["mob"].texture;
        this.sprite = new Sprite(this.texture);
        this.sprite.scale.x = globalSpriteScale;
        this.sprite.scale.y = globalSpriteScale;
        this.sprite.x = x;
        this.sprite.y = y;

        app.stage.addChild(this.sprite);
    }
}
