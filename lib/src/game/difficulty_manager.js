import { incrementGlobalSpeedMultiplier } from "../utils/game_infos";

export default class DifficultyManager {
    constructor () {
        this.timer = 0;
    }

    update(deltaTime) {
        this.timer += deltaTime;
        
        if(this.timer >= 1000) {
            incrementGlobalSpeedMultiplier(0.25);
            this.timer = 0;
        }
    }
}
