import { globalHeight, scaleSprite, globalSpriteScale, globalSpeedMultiplier } from '../utils/game_infos';
import { Sprite, resources, app } from './../pixi_application';

export default class Background {
    constructor() {
        this.skyTexture = resources["sky"].texture;
        this.sandTexture = resources["sand"].texture;
        this.dunesTexture = resources["dunes"].texture;
        this.mountainsTexture = resources["mountains"].texture;
        this.horizonTexture = resources["horizon"].texture;

        this.skySprite = new Sprite(this.skyTexture);
        scaleSprite(this.skySprite);
        app.stage.addChild(this.skySprite);

        this.horizonSprites = new BackgroundStrip(this.horizonTexture, 0.5);
        this.mountainsSprites = new BackgroundStrip(this.mountainsTexture, 0.75);
        this.dunesSprites = new BackgroundStrip(this.dunesTexture, 1.3);
        this.sandSprites = new BackgroundStrip(this.sandTexture, 3);

        this.horizonSprites.posY = globalHeight - this.horizonTexture.height * globalSpriteScale ;
        this.mountainsSprites.posY = globalHeight - this.mountainsTexture.height * globalSpriteScale ;
        this.dunesSprites.posY = globalHeight - this.dunesTexture.height * globalSpriteScale ;
        this.sandSprites.posY = globalHeight - this.sandTexture.height * globalSpriteScale;
    }

    update(deltaTime) {
        this.sandSprites.update(deltaTime);
        this.dunesSprites.update(deltaTime);
        this.mountainsSprites.update(deltaTime);
        this.horizonSprites.update(deltaTime);
    }
}

class BackgroundStrip {
    constructor(texture, speed) {
        this.spriteA = new Sprite(texture);
        this.spriteB = new Sprite(texture);

        this.speed = speed;

        scaleSprite(this.spriteA);
        scaleSprite(this.spriteB);

        this.margin = 7;
        this.spriteB.x = this.spriteB.width-this.margin;

        app.stage.addChild(this.spriteA);
        app.stage.addChild(this.spriteB);
    }

    set posY(value) {
        this.spriteA.y = value;
        this.spriteB.y = value;
    }

    update(deltaTime) {
        this.spriteA.x -= deltaTime * (this.speed * globalSpeedMultiplier);
        this.spriteB.x -= deltaTime * (this.speed * globalSpeedMultiplier);

        this.resetPosXIfNeeded(this.spriteA);
        this.resetPosXIfNeeded(this.spriteB);
    }

    resetPosXIfNeeded(sprite) {
        if (sprite.x < -sprite.width) {
            sprite.x = sprite.width-(this.margin*2);
        }
    }
}
