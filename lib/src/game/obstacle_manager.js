import { globalHeight, globalSpeedMultiplier, globalWidth } from "../utils/game_infos";
import { hitTestRectangle } from "../utils/hit_test";
import Obstacle from "./obstacle";

export default class ObstacleManager {
    constructor(collisionCallback) {
        let y = globalHeight-100;
        let a = new Obstacle(512, y),
             b = new Obstacle(1024+50, y);
            // c = new Obstacle(450, y),
            // d = new Obstacle(600, y);
        //this.obstacles = [a, b, c, d];
        this.minX = globalWidth * 0.75;
        this.maxX = globalWidth + 200;
        this.obstacles = [a, b];
        this.collisionCallback = collisionCallback;

        this.resetObstacleAtPosition(b);
    }

    updateObstacles(deltaTime) {
        for(let i =  0; i < this.obstacles.length; ++i ) {
            let obstacle = this.obstacles[i];
            obstacle.move(deltaTime);

            if(obstacle.sprite.x < 0) {
                //delete this.obstacles[i];
                //obstacle.sprite.x = 1024+50;
                this.resetObstacleAtPosition(obstacle);
            }
        }
    }

    collisionUpdate(player) {
        for(let i in this.obstacles ) {
            let hitInfos = hitTestRectangle(player.collider, this.obstacles[i].sprite);

            if(hitInfos.collides) {
                this.collisionCallback();
            }
        }
    }

    resetObstacleAtPosition(obstacle) {
        let range = this.maxX - this.minX;
        let randomRange = this.minX + range * Math.random() * globalSpeedMultiplier;
        let x = globalWidth + randomRange;
        obstacle.sprite.x = x;
    }

}
