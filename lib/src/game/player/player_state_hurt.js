import sound from './../../utils/sound';

export default class PlayerStateHurt {
    constructor (sprite) {
        this.active = false;
        this.time = 0;
        this.maxTime = 1000;
        this.sprite = sprite;
        this.hurtSound = new sound('../../../../sounds/hurt.wav');
    }

    updateIfNecessary(deltaTime) {
        if(this.active && this.time < this.maxTime) {
            this.time += deltaTime * 3;

            let alpha = Math.cos((this.time / this.maxTime) * Math.PI * 7);
            if (alpha < 0) {
                alpha = -alpha;
            }
            this.sprite.alpha = alpha;
            if(this.time >= this.maxTime) {
                this.active = false;
                this.time = 0;
                this.sprite.alpha = 1;
            }
        }
    }

    activate() {
        this.active = true;
        this.time = 0;
        this.hurtSound.play();
    }

    get isActive() {
        return this.active;
    }

}

