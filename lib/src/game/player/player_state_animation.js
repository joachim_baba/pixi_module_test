import { Rectangle } from './../../pixi_application';
import sound from './../../utils/sound';

export default class PlayerStateAnimation {
    constructor(sprite, texture) {
        this.sprite = sprite;
        this.texture = texture;

        this.idle = new Rectangle(0, 0, 20, 38);
        this.startJump = new Rectangle(20, 0, 20, 38);
        this.inJump = new Rectangle(40, 0, 20, 38);
        this.touchGround = new Rectangle(60, 0, 20, 38);
        this.frames = [this.idle, this.startJump, this.inJump, this.touchGround];
        this.groundSound = new sound('../../../../sounds/ground.wav');

        this.state = 0;
        this.mustUpdate = true;
    }

    update() {
        //console.log(this.sprite.vy);
        let state = this.state
        let velocity = this.sprite.vy;

        let startJumppTransition = state == 0 && velocity < -10;
        let inJumpTransition = state == 1 && velocity >= -8;
        let touchGroundTransition = state == 2 && velocity >= 14;
        let idleTransition = state == 3 && velocity >= 20;

        if (startJumppTransition || inJumpTransition || touchGroundTransition || idleTransition) {
            if(touchGroundTransition) {
                this.groundSound.play();
            }
            this.state++;
            if (this.state > 3) {
                this.state = 0;
            }
            this.mustUpdate = true;
            //console.log("state "+this.state);
        }

        this.updateTextureIfNecessary();
    }

    updateTextureIfNecessary() {
        if (this.mustUpdate) {
            this.mustUpdate = false;
            let frame = this.frames[this.state];
            this.texture.frame = frame;
        }
    }
}

