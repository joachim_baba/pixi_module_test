'use strict';
import { Sprite, resources, app } from './../../pixi_application';
import { PlayerStateJump } from './player_state_jump';
import PlayerStateHurt from './player_state_hurt';
import PlayerStateAnimation from './player_state_animation';
import { globalSpriteScale } from '../../utils/game_infos';

class Collider {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
    }
}

export default class Player {

    constructor() {
        this.texture = resources["character"].texture;

        this.sprite = new Sprite(this.texture);
        this.sprite.scale.x = globalSpriteScale;
        this.sprite.scale.y = globalSpriteScale;

        this.sprite.position.set(96, 96);

        this.colliderOffset = 25;
        this.colliderHalfOffset = this.colliderOffset * 0.5;
        this.collider = new Collider();
        this.collider.x = this.sprite.x + this.colliderHalfOffset;
        this.collider.y = this.sprite.y;
        this.collider.width = 80 - this.colliderOffset;
        this.collider.height = 38 * 4;

        app.stage.addChild(this.sprite);

        /*this.rectangle = new Graphics();
        this.rectangle.lineStyle(2, 0xa400a4, 1);
        this.rectangle.drawRect(0, 0, this.collider.width, this.collider.height);
        this.rectangle.endFill();
        this.rectangle.x = this.collider.x;
        this.rectangle.y = this.collider.y;

        app.stage.addChild(this.rectangle);*/

        this.initializePlayerState();
    }

    initializePlayerState() {
        this.stateJump = new PlayerStateJump(this);
        this.stateHurt = new PlayerStateHurt(this.sprite);
        this.stateAnimation = new PlayerStateAnimation(this.sprite, this.texture);
    }

    update(deltaTime) {
        this.stateJump.update(deltaTime);
        this.stateHurt.updateIfNecessary(deltaTime);
        this.stateAnimation.update();
        this.collider.x = this.sprite.x + this.colliderHalfOffset;
        this.collider.y = this.sprite.y;
        /*this.rectangle.x = this.collider.x;
        this.rectangle.y = this.collider.y;*/
    }

    onGroundHit() {
        this.stateJump.onGroundHit();
    };

    onObstacleHit() {
        this.stateHurt.activate();
    }

    get isHurtActive() {
        return this.stateHurt.active;
    }
}
