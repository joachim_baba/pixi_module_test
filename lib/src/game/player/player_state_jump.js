"use strict";
import keyboard from './../../utils/keyboard';
import sound from './../../utils/sound';

function PlayerStateJump(player) {
    this.player = player;
    this.sprite = player.sprite;
    this.originHeight = player.sprite.y;
    let _this = this;
    
    this.jumpSound = new sound('../../../../sounds/jump.wav');
    this.jumpSound.volume = 0.2;

    let gravity = 8;
    let isGrounded = true;
    _this.sprite.vy = 0;

    this.update = function (deltaTime) {
        deltaTime *= 0.05;
        _this.sprite.vy += gravity * deltaTime;

        if (_this.sprite.vy > 20) {
            _this.sprite.vy = 20
        }
        _this.sprite.y += _this.sprite.vy;

    };

    this.upKey = keyboard("ArrowUp");
    this.upKey.press = function () {
        if(isGrounded) {
            isGrounded = false;
            _this.sprite.vy = -Math.sqrt( 17* gravity);
            _this.jumpSound.play();
        }
    };

    this.onGroundHit = function () {
        isGrounded = true;
    };
}

export { PlayerStateJump };
