var globalWidth = 512, globalHeight = 256, globalSpriteScale = 4, globalSpeedMultiplier = 1;

function scaleSprite(sprite) {
    sprite.scale.x = globalSpriteScale;
    sprite.scale.y = globalSpriteScale;
}

function incrementGlobalSpeedMultiplier(value) {
    globalSpeedMultiplier += value;
}

export {globalWidth, globalHeight, globalSpriteScale, globalSpeedMultiplier, scaleSprite, incrementGlobalSpeedMultiplier};
