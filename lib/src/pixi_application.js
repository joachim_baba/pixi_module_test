'use strict';

import {PIXI} from './../pixi/pixi.min'
import {globalWidth, globalHeight} from './utils/game_infos';

// code habituellement présent dans le html
let type = "WebGL"
if(!PIXI.utils.isWebGLSupported()){
  type = "canvas"
}

PIXI.utils.sayHello(type);
// ------------

let Application = PIXI.Application,
  Sprite = PIXI.Sprite,
  Graphics = PIXI.Graphics,
  Rectangle = PIXI.Rectangle;
const loader = PIXI.Loader.shared,
  resources = PIXI.Loader.shared.resources;

let app = new Application({ width: globalWidth, height: globalHeight });
app.renderer.backgroundColor = 0x004400;
PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;


export { Application, Sprite, Graphics, Rectangle, loader, resources, app };
