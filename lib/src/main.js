'use strict';
import { loader, app } from './pixi_application';
import sound from './utils/sound';
import Game from './game/game';
import keyboard from './utils/keyboard';

document.body.appendChild(app.view);

loader
  .add("character", "../images/character_sheet.png")
  .add("sand", "../images/sand.png")
  .add("dunes", "../images/dunes.png")
  .add("mountains", "../images/mountains.png")
  .add("horizon", "../images/horizon.png")
  .add("sky", "../images/sky.png")
  .add("mob", "../images/mob.png")
  .load(setup);

let game;
let update = false, spaceKey;

function setup() {
  game = new Game();

  app.ticker.add(deltaTime => gameLoop(deltaTime));

  let music = new sound('../sounds/main_music.mp3');
  music.sound.volume = 0.3;
  music.loop();
  
  spaceKey = keyboard(" ");
  spaceKey.press = function () {
    update = !update;
    if(update == false) {
      music.stop();
    } else{
      music.play();
    }
  };
}

function gameLoop(deltaTime) {
  if(update) {
    game.update(deltaTime);
  
    game.collisionUpdate();
  }
}
